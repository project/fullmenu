# Full menu

- Output the full menu tree without using expanded choice.

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- This module help to arrange the menu smoothly.

## Requirements

- This module requires no modules outside of Drupal core.

## Installation

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Configuration

- Enable the Full menu module.
- output the full menu tree without using expanded choice,
  Then I could use superfish for main navigation.
- replace the block title of "system_menu_block" with,
  parent menu item name.
- Then the left sidebar menu block have a meaning title.

## Maintainers

- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
- thoward ge -  [g089h515r806](https://www.drupal.org/u/timmillwood)
